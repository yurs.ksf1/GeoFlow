
# Register your models here.
from django.contrib import admin
from .models import Acta, Anexo, Balance, Coordinador, Ficha_individuo, Expediente, Solicitud, \
    Seguimiento, Solicitante, Tecnico, Visita, ambProcess

admin.site.register(Acta)
admin.site.register(Anexo)
admin.site.register(Balance)
#admin.site.register(Coordinacion)
admin.site.register(Coordinador)
admin.site.register(Ficha_individuo)
admin.site.register(Expediente)
admin.site.register(Seguimiento)
admin.site.register(Solicitante)
admin.site.register(Solicitud)
admin.site.register(Tecnico)
admin.site.register(Visita)


admin.site.register(ambProcess)