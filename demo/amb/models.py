from django.db import models
from viewflow.models import Process
from django.contrib.auth.models import User

# Create your models here.
from django.core.validators import RegexValidator
#from django.contrib.gis.db import models


class Informacion(models.Model):
    identificacion = models.IntegerField()
    nombre = models.CharField(max_length=200)
    telefono = models.CharField(max_length=10)
    direccion = models.CharField(max_length=100)
    mail = models.CharField(max_length=150)

    class Meta:
        abstract = True

    def __str__(self):
        return "%s %s" % (self.identificacion, self.nombre)

        # class Tipo(Informacion):
        # administrativo, coordinacioin (id, nombre)



class Solicitante(Informacion):
    barrio = models.CharField(max_length=100)

    def __str__(self):
        return self.id_solicitante


class Balance(models.Model):
    id_balance = models.CharField(primary_key=True, max_length=8, validators=[RegexValidator(r'^\d{1,8}$')])
    descripcion = models.CharField(max_length=200)
    valor_compensado = models.DecimalField(max_digits=8, decimal_places=2)
    valor_afectacion = models.DecimalField(max_digits=8, decimal_places=2)
    valor_balance = models.DecimalField(max_digits=8, decimal_places=2)



class Timestampable(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True



class Informe_tecnico(Timestampable):
    id_informe = models.CharField(primary_key=True, max_length=8, validators=[RegexValidator(r'^\d{1,8}$')])
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300)
    Informe = models.FilePathField(null=True)


class Expediente(models.Model):
    id_expediente = models.CharField(primary_key=True, max_length=5, validators=[RegexValidator(r'^\d{1,5}$')])
    nombre = models.CharField(max_length=200)
    resolucion = models.CharField(max_length=12, blank=True, null=True)
    autorizacion = models.CharField(max_length=12, blank=True, null=True)



class Recaudo(models.Model):
    numero_recaudo = models.IntegerField(primary_key=True)
    banco = models.CharField(max_length=200)
    valor = models.DecimalField(max_digits=8, decimal_places=2)


class Solicitud(models.Model):
    id_solicitud = models.CharField(primary_key=True, max_length=10)
    nombre = models.CharField(max_length=200)  # Agregar en el modelo!!! Importante!!!!
    id_expedinte = models.ForeignKey(Expediente, blank=True, null=True, on_delete=models.CASCADE)
    id_solicitante = models.ForeignKey(Solicitante, blank=True, null=True, on_delete=models.CASCADE)
    direccion = models.CharField(max_length=100, default=' ')
    barrio = models.CharField(max_length=100, default=' ')
    municipio = models.CharField(max_length=100, default=' ')
    fecha = models.DateField(blank=True, null=True)  # Agregar!

    def __str__(self):
        return self.id_solicitud


class Anexo(models.Model):
    id_anexo = models.CharField(primary_key=True, max_length=12)
    nombre = models.CharField(max_length=200)
    anexo = models.FilePathField(null=True)
    id_solicitud = models.ForeignKey(Solicitud, on_delete=models.CASCADE)



class Ficha_individuo(models.Model):
    id_arbol = models.CharField(primary_key=True, max_length=8)
    #coordenadas!!! geodjango con point
    #coordenadas = Point(x=...., y=...., z=0, srid=....) #LocationPoint()
    latitud = models.DecimalField(max_digits=10, decimal_places=6, null=True)
    longitud = models.DecimalField(max_digits=10, decimal_places=6, null=True)
    nombre = models.CharField(max_length=100)
    familia = models.CharField(max_length=100)
    estado = models.CharField(max_length=300)
    altura = models.FloatField()
    dap = models.FloatField()
    valor = models.DecimalField(max_digits=8, decimal_places=2)



class Visita(models.Model):
    id_visita = models.CharField(primary_key=True, max_length=5, validators=[RegexValidator(r'^\d{1,10}$')])
    detalles = models.CharField(max_length=300)
    id_arbol = models.ManyToManyField(Ficha_individuo)
    id_solicitud = models.ForeignKey(Solicitud, blank=True, null=True, on_delete=models.CASCADE)
    fecha = models.DateField()


class Empleado(Informacion):
    id_solicitud = models. ManyToManyField(Solicitud, blank=True)
    #id_coordinacion = models.ManyToManyField(Coordinacion, blank=True)
    COORDINACIONES = (
        ('01', 'Flora'),
        ('02', 'Fauna'),
        ('03', 'Hidricos'),
        ('04', 'Residuos'),
        ('05', 'Aire'),
    )
    coordinacion = models.CharField(max_length=1, choices=COORDINACIONES,default='01')

    class Meta:
        abstract = True


class Coordinador(Empleado):
    pass


class Tecnico(Empleado):
    id_visita = models.ForeignKey(Visita, blank=True, null=True, on_delete=models.CASCADE)

    # Lo que me va a retornar al hacer un query, para que deje de ser "objeto <tecnico>"
    # def __str__(self):
    #    return self.nombre
    # Necesito nombre de la solicitud!!!! D:


class Seguimiento(models.Model):
    id_seguimiento = models.CharField(primary_key=True, max_length=8, validators=[RegexValidator(r'^\d{1,8}$')])
    id_balance = models.OneToOneField(Balance, blank=True, null=True, on_delete=models.CASCADE)
    id_visita = models.ForeignKey(Visita, blank=True, null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=300)
    adjunto = models.FilePathField(null=True)


class Acta(Timestampable):
    id_acta = models.CharField(primary_key=True, max_length=5)
    id_visita = models.OneToOneField(Visita,on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=300)
    acta = models.FilePathField(null=True)



class ambProcess(Process):
    text = models.CharField(max_length=150)
    people = models.ForeignKey(User)
    approved = models.BooleanField(default=False)
    verificaInfo = models.BooleanField(default=False)
    infoCompleta = models.BooleanField(default=False)
    pagoRealizado = models.BooleanField(default=False)
    agendarVisita = models.DateField(blank=True, null=True)    
    realizaVisita = models.BooleanField(default=False)  
    #coo = models.CharField(max_length=150)
    #tec = models.CharField(max_length=150)

    #empleado = models.ForeignKey(Acta, on_delete=models.CASCADE)






'''
    def is_normal_post(self):
        try:
            if self.shipment.carrier:
                return self.shipment.carrier.is_default()
            else:
                return None
        except (Shipment.DoesNotExist, Carrier.DoesNotExist):
            return None

    def need_extra_insurance(self):
        try:
            return self.shipment.need_insurance
        except Shipment.DoesNotExist:
            return None
'''