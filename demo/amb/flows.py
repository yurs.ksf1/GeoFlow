from viewflow import flow
from viewflow.base import this, Flow
from viewflow.flow.views import CreateProcessView, UpdateProcessView

from .models import ambProcess, Solicitante

from viewflow import frontend

@frontend.register
class ambFlow(Flow):
    process_class = ambProcess

    start = (
        flow.Start(
            CreateProcessView,
            #fields=["text"]
            fields=["text","people"]
        ).Permission(
            auto_create=True
        ).Next(this.approve)
    )

    approve = (
        flow.View(
            UpdateProcessView,
           # fields=["approved","empleado"]
            fields=["approved"]
        ).Permission(
            auto_create=True
        )
        .Assign(lambda activation: activation.process.people)
        .Next(this.Verificar)
    )

    Verificar = (
        flow.View(
            UpdateProcessView,
            fields=["verificaInfo", "infoCompleta", "pagoRealizado"]
        ).Permission(
            auto_create=True
        ).Next(this.check_approve)
    )

    check_approve = (
        flow.If(lambda activation: activation.process.infoCompleta and activation.process.pagoRealizado )
        .Then(this.AgVisita)
        .Else(this.actaRequerimiento)
    )

    actaRequerimiento = (
        flow.View(
            UpdateProcessView,
            fields=["infoCompleta", "pagoRealizado"]   
        )
        .Assign(username='employee')
        .Next(this.Verificar)

        )


    AgVisita = (
         flow.View(
            UpdateProcessView,
            fields=["agendarVisita"])
        .Assign(lambda act: act.process.created_by)
        .Next(this.DoVisita)

        )    


    DoVisita = (
         flow.View(
            UpdateProcessView,
            fields=["realizaVisita"])        
        .Assign(lambda act: act.process.created_by)
        .Next(this.end)

        )



    send = (
        flow.Handler(
            this.send_hello_world_request
        ).Next(this.end)
    )

    end = flow.End()

    def send_hello_world_request(self, activation):
        print(activation.process.text)